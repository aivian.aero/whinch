import pdb

import numpy as np
import scipy.optimize

import matplotlib.pyplot as plt

def fastcross(a,b):
    c1 = a[1] * b[2] - b[1] * a[2]
    c2 = b[0] * a[2] - a[0] * b[2]
    c3 = a[0] * b[1] - b[0] * a[1]
    return np.array([c1, c2, c3])

def nodes_from_X(X, condition, cable):
    """Compute the node info from the optimization vector
    """
    phi = X[0:cable['n']]
    theta = X[cable['n']:cable['n'] * 2]
    one_segments = np.vstack([
        np.cos(theta) * np.cos(phi), np.sin(phi), np.sin(theta) * np.cos(phi)]).T
    #one_segments = np.reshape(X[0:-1], (cable['n'] - 1, 3))
    #one_segments = np.einsum(
    #    'ij,i->ij', one_segments, 1 / np.linalg.norm(one_segments, axis=1))
    L = X[-1] * cable['length']
    dx_nodes = one_segments * L / cable['n']
    x_nodes = (
        np.vstack((np.zeros((3,)), np.cumsum(dx_nodes, axis=0))) +
        condition['r_winch'])
    return x_nodes

def node_calculations(x, environment, cable, condition):
    r_square = condition['r_aircraft'] @ condition['r_aircraft']
    omega = fastcross(
        condition['r_aircraft'] / r_square, condition['v_aircraft'])

    theta = x[0]
    psi = x[1]
    L_scale = x[2]
    T = np.array([
        np.cos(theta) * np.cos(psi),
        np.cos(theta) * np.sin(psi),
        np.sin(theta)]) * condition['T_winch']
    l_unstrained = L_scale * cable['length'] / (cable['n'] - 1)
    F_gravity = np.array([0, 0, cable['mass'] * environment['g']])
    x_test_nodes = np.zeros((cable['n'], 3))
    node_forces = np.zeros((cable['n'], 3))
    node_forces[0] = T
    for idx in range(1,cable['n']):
        node_acceleration = fastcross(
            omega, fastcross(omega, x_test_nodes[idx - 1]))
        node_velocity = (
            (condition['v_aircraft'] @ condition['r_aircraft'] / r_square) *
            condition['r_aircraft'] / r_square +
            fastcross(omega, x_test_nodes[idx - 1]))
        F_drag = (
            -0.5 * environment['rho'] * cable['A'] * cable['Cd'] *
            node_velocity @ node_velocity) * l_unstrained

        isgrav = np.clip(-x_test_nodes[idx - 1, 2] / 10, -1, 1)
        T = cable['mass'] * node_acceleration + T - F_drag - F_gravity * isgrav
        T_norm = np.linalg.norm(T)
        l_strained = (T_norm / cable['EA'] + 1) * l_unstrained
        x_test_nodes[idx] = x_test_nodes[idx - 1] + l_strained * T / T_norm
        node_forces[idx] = T

    return x_test_nodes, node_forces

def cost(X, info):
    x_test_nodes = node_calculations(
        X,
        info['environment'],
        info['cable'],
        info['condition'])[0]
    return np.linalg.norm(x_test_nodes[-1] - info['condition']['r_aircraft'])


if __name__ == '__main__':
    L = 2000
    n = 10
    l_unstrained = L / (n - 1)
    E = 172e9
    A = np.pi * (3/8 * 25.4 / 1000)**2 / 4
    Cd = 1.0
    g = 9.806

    v_wind = np.zeros((n, 3))
    T_winch = 2500

    r_aircraft = np.array([-L, 0, 0])
    v_aircraft = np.zeros((3,))

    r_winch = np.array([0, 0, 0])
    v_winch = np.zeros((3,))

    m = 0.0506 * L / n

    condition = {
        'r_aircraft': r_aircraft,
        'v_aircraft': v_aircraft,
        'r_winch': r_winch,
        'T_winch': T_winch,
    }
    cable = {
        'n': n,
        'A': A,
        'E': E,
        'EA': E * A,
        'mass': m,
        'Cd': 1.0,
        'l_unstrained': l_unstrained,
        'length': L
    }
    environment = {
        'g': 9.806,
        'rho': 1.0,
        'wind': np.zeros((3,)),
    }
    info = {
        'condition': condition,
        'cable': cable,
        'environment': environment,
    }

    t_nodes = np.linspace(
        condition['r_aircraft'], condition['r_winch'], cable['n'])
    t_nodes = np.tile(np.array([1, 0, 0]), (cable['n'] - 1, 1))
    phi = np.ones((cable['n'],)) * np.pi
    theta = np.zeros((cable['n'],))
    X = np.hstack((0, np.pi, 1))
    bounds = scipy.optimize.Bounds(
        [0, 0, 0.1], [2.0 * np.pi, 2.0 * np.pi, 1])
    opt = scipy.optimize.minimize(cost, X, info, tol=1e-6, bounds=bounds)
    x_nodes, F_nodes = node_calculations(
        opt['x'],
        info['environment'],
        info['cable'],
        info['condition'])
    plt.scatter(x_nodes[:, 0], -x_nodes[:, 2])
    plt.quiver(x_nodes[:, 0], -x_nodes[:, 2], F_nodes[:, 0], -F_nodes[:, 2])
    plt.axis('equal')
    plt.grid()
    plt.show()

    aircraft_path = []
    m_aircraft = 660.0 / 2.2
    dt = 0.2
    import time
    for idx in range(300):
        aircraft_path.append(r_aircraft)
        t_node_start = time.time()
        x_nodes, F_nodes = node_calculations(
            opt['x'],
            info['environment'],
            info['cable'],
            info['condition'])
        t_nodes_done = time.time()

        F_tug = F_nodes[-1]

        t_dyn_start = time.time()
        airspeed = np.linalg.norm(v_aircraft)
        if airspeed < 1:
            L_aircraft = np.zeros((3,))
            D_aircraft = np.zeros((3,))
        else:
            if r_aircraft[2] > -0.1:
                CL = 1.0
            else:
                CL = np.clip(
                    1.0 + 0.025 * (airspeed - 30) + 0.001 * v_aircraft @ a_aircraft, 0.5, 1.5)
            L_aircraft = (
                0.5 * environment['rho'] * airspeed ** 2 * 10.0 * CL *
                fastcross(v_aircraft / airspeed, [0, -1, 0]))
            D_aircraft = -(
                0.5 * environment['rho'] * airspeed ** 2.0 * 10.0 * CL / 20 *
                v_aircraft / airspeed)

        F_aircraft = -F_tug + L_aircraft + D_aircraft

        F_aircraft[2] += m_aircraft * environment['g']
        if condition['r_aircraft'][2] > -0.1:
            if F_aircraft[2] > 0:
                F_aircraft[2] = 0

        a_aircraft = F_aircraft / m_aircraft
        v_aircraft = v_aircraft + a_aircraft * dt
        r_aircraft = r_aircraft + v_aircraft * dt
        condition['v_aircraft'] = v_aircraft
        condition['r_aircraft'] = r_aircraft
        t_dyn_done = time.time()

        t_opt_start = time.time()
        opt = scipy.optimize.minimize(
            cost, opt['x'], info,
            tol=1e-3)
        t_opt_done = time.time()
        pdb.set_trace()

        print('times: {:0.3f}'.format(idx * dt))
        print('  node info: {:0.6f}'.format(t_nodes_done - t_node_start))
        print('  dynamics:  {:0.6f}'.format(t_dyn_done - t_dyn_start))
        print('  optimize:  {:0.6f}'.format(t_opt_done - t_opt_start))

    aircraft_path = np.array(aircraft_path + [r_aircraft])
    x_nodes, F_nodes = node_calculations(
        opt['x'],
        info['environment'],
        info['cable'],
        info['condition'])
    plt.plot(aircraft_path[:, 0], -aircraft_path[:, 2])
    plt.scatter(x_nodes[:, 0], -x_nodes[:, 2])
    plt.quiver(x_nodes[:, 0], -x_nodes[:, 2], F_nodes[:, 0], -F_nodes[:, 2])
    plt.axis('equal')
    plt.grid()
    plt.show()
