#!/usr/bin/env python3
import pdb

import socket

import numpy as np
import scipy.optimize

import rclpy.node
from geometry_msgs.msg import Vector3Stamped
from std_msgs.msg import Header

from geometry.quaternion import from_euler, UnitQuaternion
from geodesy.conversions import ned_to_lla, lla_to_ned

from qs_cable import cost, node_calculations

class FlightgearInterface(rclpy.node.Node):
    def __init__(self, ip='127.0.0.1', rx_port=6789, tx_port=6790):
        super().__init__('flightgear_interface')
        self._rx_port = rx_port
        self._tx_port = tx_port
        self._ip = ip
        self.connect()

        self._ias = None
        self._lla = None
        self._V = None
        self._winch_lla = None
        self._going = False

        self._tether_state = None

        self._condition = {
            'r_aircraft': None,
            'v_aircraft': None,
            'r_winch': None,
            'T_winch': None,
        }
        # 3/8" spectra
        L = 2000
        n = 10
        l_unstrained = L / (n - 1)
        E = 172e9
        A = np.pi * (3/8 * 25.4 / 1000)**2 / 4
        Cd = 1.0
        g = 9.806
        m = 0.0506 * L / n
        self._cable = {
            'n': n,
            'A': A,
            'E': E,
            'EA': E * A,
            'mass': m,
            'Cd': 1.0,
            'l_unstrained': l_unstrained,
            'length': L
        }
        self._environment = {
            'g': 9.806,
            'rho': 1.0,
        }
        self._info = {
            'condition': self._condition,
            'cable': self._cable,
            'environment': self._environment,
        }

        self._comm_service_timer = self.create_timer(0.05, self._service_comms)
        self._place_winch_sub = self.create_subscription(
            Vector3Stamped,
            '/winch/location',
            self._place_winch_callback,
            10)
        self._start_winch_sub = self.create_subscription(
            Header,
            '/winch/go',
            self._go_go_go_callback,
            10)
        self._stop_winch_sub = self.create_subscription(
            Header,
            '/winch/stop',
            self._stop_stop_stop_callback,
            10)

    def connect(self):
        self._connection = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._connection.bind((self._ip, self._rx_port))

    def _service_comms(self):
        rx_data = self._connection.recvfrom(1024)
        if len(rx_data) == 0:
            return
        fg_data = rx_data[0].decode().strip().split(',')
        self._ias = float(fg_data[0]) * 0.5144444
        self._rho = float(fg_data[1]) * 515.4
        self._lon = np.deg2rad(float(fg_data[2]))
        self._lat = np.deg2rad(float(fg_data[3]))
        self._alt = float(fg_data[4]) / 3.28
        self._lla = np.array([self._lat, self._lon, self._alt])
        self._vn = float(fg_data[5]) / 3.28
        self._ve = float(fg_data[6]) / 3.28
        self._vd = float(fg_data[7]) / 3.28
        self._V = np.array([self._vn, self._ve, self._vd])
        self._phi = float(fg_data[8])
        self._theta = float(fg_data[9])
        self._psi = float(fg_data[10])
        self._orientation = from_euler([self._phi, self._theta, self._psi])
        self._mass = float(fg_data[11]) * 14.59

        self._environment['rho'] = self._rho

        if self._winch_lla is not None:
            self._X_aircraft = lla_to_ned(self._lla, self._winch_lla)
            self._condition['r_aircraft'] = self._X_aircraft[0]
            self._condition['v_aircraft'] = self._V

        if self._going:
            opt = scipy.optimize.minimize(
                cost, self._tether_state, self._info, tol=1e-6)
            #print(opt['fun'])
            self._tether_state = opt['x']
            x_nodes, F_nodes = node_calculations(
                self._tether_state,
                self._info['environment'],
                self._info['cable'],
                self._info['condition'])
            T = np.linalg.norm(F_nodes[-1])
            one_T = -self._orientation.rot(F_nodes[-1] / T)
            output_string = '{:1.8f},{:1.8f},{:1.8f},{:5.3f}\n'.format(
                one_T[0], one_T[1], one_T[2], T)
            #print(F_nodes[-1])
            self._connection.sendto(
                output_string.encode(), (self._ip, self._tx_port))

            if (np.array([1, 0, 0]) @ one_T) < np.cos(np.deg2rad(75)):
                print('released')
                self._stop_stop_stop_callback(None)

    def _place_winch_callback(self, msg):
        if self._going:
            return
        aircraft_to_winch = np.array([msg.vector.x, msg.vector.y, msg.vector.z])
        ned_to_winch = from_euler([0, 0, self._psi]).rot(
            aircraft_to_winch, False)
        self._winch_lla = ned_to_lla(ned_to_winch, self._lla[None])
        self._tether_state = None
        self._condition['r_aircraft'] = lla_to_ned(
            self._lla, self._winch_lla)[0]
        self._condition['v_aircraft'] = self._V
        self._condition['r_winch'] = np.zeros((3,))
        self._condition['T_winch'] = self._mass * 9.806 * 0.75 / 4.4
        X = np.hstack((0, np.fmod(np.pi + self._psi, 2.0 * np.pi), 1))
        bounds = scipy.optimize.Bounds(
            [0, 0, 0.1], [2.0 * np.pi, 2.0 * np.pi, 1])
        opt = scipy.optimize.minimize(
            cost, X, self._info, tol=1e-6, bounds=bounds)
        self._tether_state = opt['x']

    def _go_go_go_callback(self, msg):
        if not self._going and not self._ias > 5:
            self._countdown = self.create_timer(3.0, self._going_callback)

    def _going_callback(self):
        print('going')
        self._countdown.cancel()
        self._going = True
        self._condition['T_winch'] = self._mass * 9.806 * 0.75 / 4.4

    def _stop_stop_stop_callback(self, msg):
        print('stopping winch')
        self._going = False
        self._condition['T_winch'] = 0.0
        output_string = '{:1.8f},{:1.8f},{:1.8f},{:5.3f}\n'.format(
            1, 0, 0, 0)
        self._connection.sendto(
            output_string.encode(), (self._ip, self._tx_port))

if __name__ == '__main__':
    rclpy.init()
    this_node = FlightgearInterface()

    rclpy.spin(this_node)
    this_node.destroy_node()
    rclpy.shutdown()


